import 'dog.dart';
import 'dog_dao.dart';

Future<void> addNew(Dog dog) {
  return DogDao.insertDog(dog);
}

Future<void> saveDog(Dog dog) {
  return DogDao.updateDog(dog);
}

Future<void> delDog(Dog dog) {
  return DogDao.deletDog(dog.id);
}

Future<List<Dog>> getDogs() {
  return DogDao.dogs();
}
